import { Routes, Route, BrowserRouter } from 'react-router-dom';
import { publicRoutes } from './routes';
import 'styles/global.scss';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        {publicRoutes.map(({ path, Component }) => (
          <Route key={path} path={path} element={<Component />} />
        ))}
      </Routes>
    </BrowserRouter>
  );
}

export default App;
