import axios from 'axios';

const apiUrl = process.env.REACT_APP_BASE_API;

export default axios.create({
  baseURL: apiUrl,
  headers: {
    Accept: 'application/json'
  }
});
