export interface Product {
  id: number;
  title: string;
  description: string;
  price: string;
  discountPercentage: string;
  rating: string;
  stock: string;
  brand: string;
  category: string;
  thumbnail: string;
  images: string[];
}

export interface ProductsData {
  count: number;
  products: Product[];
}
