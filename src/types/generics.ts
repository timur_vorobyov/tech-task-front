export type Anything = any;
export type GenericAsyncFunction = (...args: Anything[]) => Promise<Anything>;
