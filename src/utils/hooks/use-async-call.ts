import { useState } from 'react';
import { Anything, GenericAsyncFunction } from 'types/generics';

const useApiCall = (asyncApiFunction: GenericAsyncFunction, args: Anything[] = []) => {
  const [data, setData] = useState<Anything>(null);
  const [error, setError] = useState<unknown>(null);
  const [isLoading, setIsLoading] = useState(true);

  const fetchMethod = async (methodArgs: Anything[] = []) => {
    setIsLoading(true);

    try {
      const response = args.length
        ? await asyncApiFunction(...args)
        : await asyncApiFunction(...methodArgs);
      setData(response);
      setIsLoading(false);
    } catch (error) {
      console.error('useApiCall error', error);
      setError(error);
    }
  };

  return [data, fetchMethod, isLoading, error];
};
export default useApiCall;
