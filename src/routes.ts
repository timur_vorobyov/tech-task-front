import HomePage from 'pages/HomePage/HomePage';
import { HOME_ROUTE } from './utils/consts';

export const publicRoutes = [
  {
    path: HOME_ROUTE,
    Component: HomePage
  }
];

export const routes = {
  api: {
    products: {
      base: 'api/products',
      getFilteredProducts: 'api/products/filter',
      export: 'api/products/export',
      saveProductsFromDummy: 'api/products/download',
      uploadProductImages: 'api/products/upload'
    }
  }
};
