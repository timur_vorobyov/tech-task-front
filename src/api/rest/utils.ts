import { Product } from 'types/product';
import { ProductDbType } from './types';

export const formatProductsList = (products: ProductDbType[]): Product[] => {
  return products.map((product) => {
    return {
      id: product.id,
      title: product.title,
      description: product.description,
      price: product.price.toString(),
      discountPercentage: product.discountPercentage.toString(),
      rating: product.rating.toString(),
      stock: product.stock.toString(),
      brand: product.brand,
      category: product.category,
      thumbnail: product.thumbnail,
      images: product.images
    };
  });
};
