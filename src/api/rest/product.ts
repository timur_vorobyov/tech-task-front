import apiClient from 'helpers/httpInterceptor';
import { Filters } from 'pages/HomePage/types';
import { routes } from 'routes';
import { Product } from 'types/product';
import { ModifyProductType } from './types';
import { formatProductsList } from './utils';

export const getFilteredProductsList = async ({
  limit,
  currentPage,
  search,
  rating,
  price,
  discount
}: Filters): Promise<{ count: number; products: Product[] }> => {
  const response = await apiClient.get(routes.api.products.getFilteredProducts, {
    params: {
      limit,
      offset: currentPage ? limit * (currentPage + 1) : 0,
      search,
      rating,
      price,
      discount
    }
  });

  return {
    count: response.data.count,
    products: formatProductsList(response.data.products)
  };
};

export const exportProducts = async ({
  limit,
  currentPage,
  search,
  rating,
  price,
  discount
}: Filters) => {
  const response = await apiClient.get(routes.api.products.export, {
    params: {
      limit,
      offset: currentPage === 1 ? 0 : currentPage * limit,
      search,
      rating,
      price,
      discount
    }
  });

  return response.data;
};

export const getProductsList = async () => {
  const response = await apiClient.get(routes.api.products.base);

  return response.data;
};

export const saveProductsFromDummy = async () => {
  const response = await apiClient.post(routes.api.products.saveProductsFromDummy);

  return response.data;
};

export const storeProduct = async (data: ModifyProductType) => {
  const response = await apiClient.post(routes.api.products.base, data);
  return response.data;
};

export const updateProduct = async (id: number, data: ModifyProductType) => {
  const response = await apiClient.put(
    routes.api.products.base + '/' + encodeURIComponent(id),
    data
  );
  return response.data;
};

export const uploadImages = async (formData: FormData) => {
  const response = await apiClient.post(routes.api.products.uploadProductImages, formData);
  return response.data;
};

export const deleteProuduct = async (id: number) => {
  const response = await apiClient.delete(routes.api.products.base + '/' + encodeURIComponent(id));

  return response.data;
};
