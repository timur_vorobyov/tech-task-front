import Slider from '@material-ui/core/Slider';
import './Range.scss';

interface RangeProps {
  min: number;
  max: number;
  selectedItem: number[];
  onRangeChange: (
    event: React.ChangeEvent<Record<string, unknown>>,
    value: number[] | number
  ) => void;
}

const Range = ({ min, max, selectedItem, onRangeChange }: RangeProps) => {
  return (
    <div className="range">
      <Slider
        value={selectedItem}
        onChange={onRangeChange}
        valueLabelDisplay="on"
        min={min}
        max={max}
        className="thumb rail track"
      />
    </div>
  );
};

export default Range;
