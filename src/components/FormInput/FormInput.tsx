import { useState } from 'react';
import './FormInput.scss';
interface FormInputProps {
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  type: string;
  placeholder: string;
  pattern: string;
  required: boolean;
  label: string;
  name: string;
  errorMessage: string;
  value: string | number;
}
const FormInput = ({
  onChange,
  label,
  errorMessage,
  value,
  name,
  type,
  placeholder,
  pattern,
  required
}: FormInputProps) => {
  const [focused, setFocused] = useState(false);
  const handleFocus = () => {
    setFocused(true);
  };
  return (
    <div className="formInput">
      <label>{label}</label>
      <input
        className={focused ? 'focused' : 'unfocused'}
        type={type}
        name={name}
        pattern={pattern}
        required={required}
        placeholder={placeholder}
        onChange={onChange}
        value={value}
        onBlur={handleFocus}
      />
      <span>{errorMessage}</span>
    </div>
  );
};

export default FormInput;
