import { RatingOption, RATING_OPTIONS } from './const';
import './Rating.scss';

interface RatingProps {
  selectedRating: number;
  onRatingClick: (value: number) => void;
}

const Rating = ({ selectedRating, onRatingClick }: RatingProps) => {
  return (
    <div className="rating">
      {RATING_OPTIONS.map(({ label, id, value }: RatingOption) => (
        <button
          onClick={() => onRatingClick(value)}
          className={selectedRating === value ? 'btn active' : 'btn'}
          key={id}
          value={value}
        >
          {label}
        </button>
      ))}
    </div>
  );
};

export default Rating;
