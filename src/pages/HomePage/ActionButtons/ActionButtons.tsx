import './ActionButtons.scss';
import Button from '@mui/material/Button';

interface ActionButtonsProps {
  onOpenProductModalClick: () => void;
  onExportFilteredProductsClick: () => void;
  onSaveProductsFromDummyClick: () => void;
  onExportAllProductsClick: () => void;
}
const ActionButtons = ({
  onOpenProductModalClick,
  onExportFilteredProductsClick,
  onSaveProductsFromDummyClick,
  onExportAllProductsClick
}: ActionButtonsProps) => {
  return (
    <div className="buttons">
      <Button className="btn" variant="outlined" onClick={() => onOpenProductModalClick()}>
        Add Product
      </Button>
      <Button className="btn" variant="outlined" onClick={() => onSaveProductsFromDummyClick()}>
        Download data
      </Button>
      <Button
        className="btn"
        onClick={() => onExportFilteredProductsClick()}
        variant="contained"
        color="success"
      >
        Export filtered products
      </Button>
      <Button
        className="btn"
        onClick={() => onExportAllProductsClick()}
        variant="contained"
        color="success"
      >
        Export all products
      </Button>
    </div>
  );
};

export default ActionButtons;
