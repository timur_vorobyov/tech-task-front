import './DeleteModal.scss';
import { Modal } from '@material-ui/core';
import Button from '@mui/material/Button';
import { ModalDeletePropertiesState } from '../types';
import { deleteProuduct } from 'api/rest/product';

interface DeleteModalProps {
  modalDeletePropertiesState: ModalDeletePropertiesState;
  id: number;
  recallProductApi: () => void;
}

const DeleteModal = ({ modalDeletePropertiesState, id, recallProductApi }: DeleteModalProps) => {
  const handleConfirmationDeleteProductModalClick = async () => {
    await deleteProuduct(id);

    modalDeletePropertiesState.setIsDeleteModalShown(false);
    recallProductApi();
  };

  return (
    <Modal
      open={modalDeletePropertiesState.isDeleteModalShown}
      onClose={() => modalDeletePropertiesState.setIsDeleteModalShown(false)}
    >
      <div className="modalContainer">
        <span className="title">Are you sure you want to delete the product?</span>
        <div className="buttons">
          <Button
            className="btn"
            variant="outlined"
            onClick={() => modalDeletePropertiesState.setIsDeleteModalShown(false)}
          >
            Cancel
          </Button>
          <Button
            className="btn"
            variant="outlined"
            color="error"
            onClick={() => {
              handleConfirmationDeleteProductModalClick();
              modalDeletePropertiesState.setIsDeleteModalShown(false);
            }}
          >
            Delete
          </Button>
        </div>
      </div>
    </Modal>
  );
};

export default DeleteModal;
