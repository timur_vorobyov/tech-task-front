import './ProductModal.scss';
import { Modal } from '@material-ui/core';
import Button from '@mui/material/Button';
import { useState } from 'react';
import { ModalPropertiesState, ProductInModal } from '../types';
import { MODAL_TYPE } from '../const';
import { storeProduct, updateProduct, uploadImages } from 'api/rest/product';
import { INPUTS } from './const';
import FormInput from 'components/FormInput/FormInput';

interface ProductModalProps {
  modalPropertiesState: ModalPropertiesState;
  productInModalState: {
    productInModal: ProductInModal;
    setProductInModal: (productInModal: ProductInModal) => void;
  };
  recallProductApi: () => void;
}
const ProductModal = ({
  modalPropertiesState,
  productInModalState,
  recallProductApi
}: ProductModalProps) => {
  const [imagesValidationMessage, setImagesValidationMessage] = useState('');
  const [thumbnailValidationMessage, setThumbnailValidationMessage] = useState('');
  const onUpdateButtonClicked = async () => {
    const formData = new FormData();
    if (productInModalState.productInModal.thumbnail) {
      formData.append('thumbnail', productInModalState.productInModal.thumbnail);
    }
    const images = productInModalState.productInModal.images;
    for (let i = 0; i < images.length; i++) {
      formData.append('images', images[i]);
    }
    const uploadedFiles: {
      thumbnail: string;
      images: string[];
    } = await uploadImages(formData);

    const dataToSave = {
      title: productInModalState.productInModal.title,
      description: productInModalState.productInModal.description,
      price: Number(productInModalState.productInModal.price),
      discountPercentage: Number(productInModalState.productInModal.discountPercentage),
      rating: Number(productInModalState.productInModal.rating),
      stock: Number(productInModalState.productInModal.stock),
      brand: productInModalState.productInModal.brand,
      category: productInModalState.productInModal.category,
      thumbnail: uploadedFiles.thumbnail,
      images: uploadedFiles.images
    };

    await updateProduct(productInModalState.productInModal.id, dataToSave);

    modalPropertiesState.setModalProperties({
      ...modalPropertiesState.modalProperties,
      isModalShown: false
    });
    recallProductApi();
  };
  const [errors, setErrors] = useState(0);

  const onCreateButtonClicked = async () => {
    const formData = new FormData();
    if (productInModalState.productInModal.thumbnail) {
      formData.append('thumbnail', productInModalState.productInModal.thumbnail);
    }
    const images = productInModalState.productInModal.images;
    for (let i = 0; i < images.length; i++) {
      formData.append('images', images[i]);
    }
    const uploadedFiles: {
      thumbnail: string;
      images: string[];
    } = await uploadImages(formData);

    const dataToSave = {
      title: productInModalState.productInModal.title,
      description: productInModalState.productInModal.description,
      price: Number(productInModalState.productInModal.price),
      discountPercentage: Number(productInModalState.productInModal.discountPercentage),
      rating: Number(productInModalState.productInModal.rating),
      stock: Number(productInModalState.productInModal.stock),
      brand: productInModalState.productInModal.brand,
      category: productInModalState.productInModal.category,
      thumbnail: uploadedFiles.thumbnail,
      images: uploadedFiles.images
    };
    await storeProduct(dataToSave);

    modalPropertiesState.setModalProperties({
      ...modalPropertiesState.modalProperties,
      isModalShown: false
    });
    recallProductApi();
  };

  const handleImages = (e: React.ChangeEvent<HTMLInputElement>) => {
    const files = e.target.files || [];
    const images = [];
    for (let i = 0; i < files.length; i++) {
      if (!files[i].name.match(/\.(jpg|jpeg|png)$/)) {
        setErrors(errors + 1);
        setImagesValidationMessage('Please select only jpg|jpeg|png formats');
        return;
      }
      images.push(files[i]);
    }

    productInModalState.setProductInModal({
      ...productInModalState.productInModal,
      images
    });
    const errAmmount = errors === 0 ? errors : errors - 1;
    setErrors(errAmmount);
    setImagesValidationMessage('');
  };

  const handleImage = (e: React.ChangeEvent<HTMLInputElement>) => {
    const files = e.target.files || [];
    if (!files[0].name.match(/\.(jpg|jpeg|png)$/)) {
      setErrors(errors + 1);
      setThumbnailValidationMessage('Please select only jpg|jpeg|png formats');
      return;
    }
    productInModalState.setProductInModal({
      ...productInModalState.productInModal,
      thumbnail: files[0]
    });
    const errAmmount = errors === 0 ? errors : errors - 1;
    setErrors(errAmmount);
    setThumbnailValidationMessage('');
  };

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.validity.patternMismatch) {
      const errAmmount = errors + 1;
      setErrors(errAmmount);
    } else {
      const errAmmount = errors === 0 ? errors : errors - 1;
      setErrors(errAmmount);
    }
    productInModalState.setProductInModal({
      ...productInModalState.productInModal,
      [e.target.name]: e.target.value
    });
  };

  return (
    <Modal
      open={modalPropertiesState.modalProperties.isModalShown}
      onClose={() =>
        modalPropertiesState.setModalProperties({
          ...modalPropertiesState.modalProperties,
          isModalShown: false
        })
      }
    >
      <div className="modalContainer">
        <form
          onSubmit={(e) => {
            e.preventDefault();
            modalPropertiesState.modalProperties.action == MODAL_TYPE.CREATE.NAME
              ? onCreateButtonClicked()
              : onUpdateButtonClicked();
          }}
        >
          {INPUTS.map((input) => {
            return (
              <FormInput
                key={input.id}
                label={input.label}
                errorMessage={input.errorMessage}
                value={
                  productInModalState.productInModal[
                    input.name as keyof typeof productInModalState.productInModal
                  ] as string | number
                }
                onChange={onChange}
                type={input.type}
                placeholder={input.placeholder}
                pattern={input.pattern}
                required={input.required}
                name={input.name}
              />
            );
          })}
          <div className="formFileInput">
            <label>Thumbnail</label>
            <input type="file" name="thumbnail" onChange={handleImage} required />
            {thumbnailValidationMessage && <p>{thumbnailValidationMessage} </p>}
          </div>
          <div className="formFileInput">
            <label>Images</label>
            <input type="file" onChange={handleImages} multiple required />
            {imagesValidationMessage && <p>{imagesValidationMessage} </p>}
          </div>
          <div className="buttons">
            <Button
              className="btn"
              variant="outlined"
              color="error"
              onClick={() =>
                modalPropertiesState.setModalProperties({
                  ...modalPropertiesState.modalProperties,
                  isModalShown: false
                })
              }
            >
              Cancel
            </Button>
            <Button className="btn" disabled={errors > 0} variant="outlined" type="submit">
              {modalPropertiesState.modalProperties.action == MODAL_TYPE.CREATE.NAME
                ? 'Save'
                : 'Update'}
            </Button>
          </div>
        </form>
      </div>
    </Modal>
  );
};

export default ProductModal;
