export const INPUTS = [
  {
    id: 1,
    name: 'title',
    type: 'text',
    placeholder: 'Title',
    errorMessage: "Title should be 3-16 characters and shouldn't include any special character!",
    label: 'Title',
    pattern: '^[A-Za-z0-9]{3,16}$',
    required: true
  },
  {
    id: 2,
    name: 'description',
    type: 'text',
    placeholder: 'Description',
    errorMessage:
      "Description should be 5-30 characters and shouldn't include any special character!",
    label: 'Description',
    pattern: '^[A-Za-z0-9]{5,30}$',
    required: true
  },
  {
    id: 3,
    name: 'price',
    type: 'text',
    placeholder: 'Price',
    errorMessage: 'Price should be 1-5 characters and should include only numbers!',
    label: 'Price',
    pattern: '^[0-9]{1,5}$',
    required: true
  },
  {
    id: 4,
    name: 'stock',
    type: 'text',
    placeholder: 'Stock',
    errorMessage: 'Stock should be 1-5 characters and should include only numbers!',
    label: 'Stock',
    pattern: '^[0-9]{1,5}$',
    required: true
  },
  {
    id: 5,
    name: 'rating',
    type: 'text',
    placeholder: 'Rating',
    errorMessage: 'Rating should be between 1 and 5 and should include only numbers!',
    label: 'Rating',
    pattern: '[1-5]+([.][0-9]+)?',
    required: true
  },
  {
    id: 6,
    name: 'brand',
    type: 'text',
    placeholder: 'Brand',
    errorMessage: "Brand should be 3-16 characters and shouldn't include any special character!",
    label: 'Brand',
    pattern: '^[A-Za-z0-9]{3,16}$',
    required: true
  },
  {
    id: 7,
    name: 'category',
    type: 'text',
    placeholder: 'Category',
    errorMessage: "Category should be 3-16 characters and shouldn't include any special character!",
    label: 'Category',
    pattern: '^[A-Za-z0-9]{3,16}$',
    required: true
  },
  {
    id: 8,
    name: 'discountPercentage',
    type: 'text',
    placeholder: 'Discount percentage',
    errorMessage: 'Discount percentage should be 1-5 characters and should include only numbers!',
    label: 'Discount percentage',
    pattern: '[0-9]+([.][0-9]{1,5})?',
    required: true
  }
];
