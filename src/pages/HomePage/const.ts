export const MIN_PRICE = 1;
export const MAX_PRICE = 5000;

export const MIN_DISCOUNT_PRICE = 1;
export const MAX_DISCOUNT_PRICE = 100;

export const INITIAL_LIMIT = 5;
export const INITIAL_CURRENT_PAGE = 0;

export const INITIAL_PRODUCT_IN_MODAL = {
  id: 0,
  title: '',
  description: '',
  price: '',
  discountPercentage: '',
  rating: '',
  stock: '',
  brand: '',
  category: '',
  thumbnail: null,
  images: []
};

export const MODAL_TYPE = {
  CREATE: {
    NAME: 'create',
    ACTION: 'Сохранить'
  },
  UPDATE: {
    NAME: 'update',
    ACTION: 'Обновить'
  }
};
