import Rating from 'components/Rating/Rating';
import { Filters } from '../types';
import './FilterPanel.scss';
import Range from 'components/Range/Range';
import SearchBar from '../SearchBar/SearchBar';
import { Button } from '@mui/material';
import {
  MAX_PRICE,
  MIN_PRICE,
  MIN_DISCOUNT_PRICE,
  MAX_DISCOUNT_PRICE,
  INITIAL_LIMIT,
  INITIAL_CURRENT_PAGE
} from '../const';

interface FilterPanelProps {
  applyFilters: (limit: number, currentPage: number) => void;
  filtersProperties: {
    filters: Filters;
    onFiltersChange: (name: string, value: string | number | number[]) => void;
  };
}
const FilterPanel = ({ applyFilters, filtersProperties }: FilterPanelProps) => {
  const onRatingClick = (value: number) => {
    if (value === filtersProperties.filters.rating) {
      filtersProperties.onFiltersChange('rating', 0);
    } else {
      filtersProperties.onFiltersChange('rating', value);
    }
  };

  const onPriceChange = (
    event: React.ChangeEvent<Record<string, unknown>> /**event is required by material*/,
    value: number[] | number
  ) => {
    if (Array.isArray(value)) {
      filtersProperties.onFiltersChange('price', value);
    }
  };

  const onDiscountChange = (
    event: React.ChangeEvent<Record<string, unknown>> /**event is required by material*/,
    value: number[] | number
  ) => {
    if (Array.isArray(value)) {
      filtersProperties.onFiltersChange('discount', value);
    }
  };

  const onSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    filtersProperties.onFiltersChange('search', event.target.value);
  };

  return (
    <div className="filterPanel">
      <div className="filter">
        <SearchBar value={filtersProperties.filters.search} changeInput={onSearchChange} />
      </div>
      <div className="filter">
        <p className="rangeTitle">Price Range</p>
        <Range
          min={MIN_PRICE}
          max={MAX_PRICE}
          selectedItem={filtersProperties.filters.price}
          onRangeChange={onPriceChange}
        />
      </div>
      <div className="filter">
        <p className="rangeTitle">Discount Range</p>
        <Range
          min={MIN_DISCOUNT_PRICE}
          max={MAX_DISCOUNT_PRICE}
          selectedItem={filtersProperties.filters.discount}
          onRangeChange={onDiscountChange}
        />
      </div>
      <div className="filter">
        <p className="ratingTitle">Star Rating</p>
        <Rating selectedRating={filtersProperties.filters.rating} onRatingClick={onRatingClick} />
      </div>
      <div className="filterBtn">
        <Button
          onClick={() => applyFilters(INITIAL_LIMIT, INITIAL_CURRENT_PAGE)}
          variant="outlined"
        >
          Filter
        </Button>
      </div>
    </div>
  );
};

export default FilterPanel;
