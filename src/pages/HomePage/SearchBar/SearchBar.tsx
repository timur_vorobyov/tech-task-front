import './SearchBar.scss';
import SearchOutlinedIcon from '@mui/icons-material/SearchOutlined';

interface SearchBarProps {
  value: string;
  changeInput: (event: React.ChangeEvent<HTMLInputElement>) => void;
}
const SearchBar = ({ value, changeInput }: SearchBarProps) => (
  <div className="search">
    <input type="text" placeholder="Search..." value={value} onChange={changeInput} />
    <SearchOutlinedIcon />
  </div>
);
export default SearchBar;
