import './Table.scss';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { TablePagination } from '@mui/material';
import { Product, ProductsData } from 'types/product';
import { INITIAL_PRODUCT_IN_MODAL } from '../const';
import { ProductInModal } from '../types';

interface BasicTableProps {
  onOpenProductModalClick: () => void;
  applyFilters: (limit: number, currentPage: number) => void;
  productsData: ProductsData;
  limit: number;
  currentPage: number;
  onOpenDeleteModalShown: () => void;
  onActionButtonClick: (value: ProductInModal) => void;
}

const BasicTable = ({
  onOpenProductModalClick,
  onOpenDeleteModalShown,
  onActionButtonClick,
  applyFilters,
  limit,
  currentPage,
  productsData
}: BasicTableProps) => {
  return (
    <div className="tableContainer">
      <TableContainer className="table">
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell className="tableCell">Tracking ID</TableCell>
              <TableCell className="tableCell">Title</TableCell>
              <TableCell className="tableCell">Description</TableCell>
              <TableCell className="tableCell">Price</TableCell>
              <TableCell className="tableCell">Rating</TableCell>
              <TableCell className="tableCell">Discount Percentage</TableCell>
              <TableCell className="tableCell">Brand</TableCell>
              <TableCell className="tableCell">Category</TableCell>
              <TableCell className="tableCell">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {productsData.products.map((product: Product) => (
              <TableRow key={product.id}>
                <TableCell className="tableCell">{product.id}</TableCell>
                <TableCell className="tableCell">
                  <div className="cellWrapper">
                    <img
                      src={
                        /^https:\/\//.test(product.thumbnail)
                          ? product.thumbnail
                          : process.env.REACT_APP_BASE_API + product.thumbnail
                      }
                      alt=""
                      className="image"
                    />
                    {product.title}
                  </div>
                </TableCell>
                <TableCell className="tableCell">{product.description}</TableCell>
                <TableCell className="tableCell">{product.price}</TableCell>
                <TableCell className="tableCell">{product.rating}</TableCell>
                <TableCell className="tableCell">{product.discountPercentage}</TableCell>
                <TableCell className="tableCell">{product.brand}</TableCell>
                <TableCell className="tableCell">{product.category}</TableCell>
                <TableCell className="tableCell">
                  <div className="action">
                    <button
                      className="status update"
                      onClick={() => {
                        onActionButtonClick({
                          ...INITIAL_PRODUCT_IN_MODAL,
                          id: product.id,
                          title: product.title,
                          description: product.description,
                          price: product.price,
                          discountPercentage: product.discountPercentage,
                          rating: product.rating,
                          stock: product.stock,
                          brand: product.brand,
                          category: product.category
                        });
                        onOpenProductModalClick();
                      }}
                    >
                      Update
                    </button>
                    <button
                      className="status remove"
                      onClick={() => {
                        onOpenDeleteModalShown();
                        onActionButtonClick({
                          ...INITIAL_PRODUCT_IN_MODAL,
                          id: product.id
                        });
                      }}
                    >
                      Delete
                    </button>
                  </div>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

      <TablePagination
        component="div"
        rowsPerPageOptions={[5, 20, 50]}
        count={productsData.count}
        page={currentPage}
        onPageChange={(event: unknown, newPage: number) => {
          applyFilters(limit, newPage++);
        }}
        rowsPerPage={limit}
        onRowsPerPageChange={(event: React.ChangeEvent<HTMLInputElement>) => {
          const limit = parseInt(event.target.value, 10);
          const page = currentPage > 0 && productsData.count < limit ? 0 : currentPage;
          applyFilters(limit, page);
        }}
      />
    </div>
  );
};

export default BasicTable;
