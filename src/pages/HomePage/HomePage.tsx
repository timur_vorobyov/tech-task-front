import { CircularProgress } from '@material-ui/core';
import { getFilteredProductsList, getProductsList, saveProductsFromDummy } from 'api/rest/product';
import { useEffect, useState } from 'react';
import { ProductsData } from 'types/product';
import useApiCall from 'utils/hooks/use-async-call';
import {
  INITIAL_CURRENT_PAGE,
  INITIAL_LIMIT,
  INITIAL_PRODUCT_IN_MODAL,
  MAX_DISCOUNT_PRICE,
  MAX_PRICE,
  MIN_DISCOUNT_PRICE,
  MIN_PRICE,
  MODAL_TYPE
} from './const';
import FilterPanel from './FilterPanel/FilterPanel';
import './HomePage.scss';
import BasicTable from './Table/Table';
import { Filters, ModalProperties, ProductInModal } from './types';
import exportFromJSON from 'export-from-json';
import ActionButtons from './ActionButtons/ActionButtons';
import DeleteModal from './DeleteModal/DeleteModal';
import ProductModal from './ProductModal/ProductModal';

const HomePage = () => {
  const [filters, setFilters] = useState<Filters>({
    limit: INITIAL_LIMIT,
    currentPage: INITIAL_CURRENT_PAGE,
    search: '',
    rating: 0,
    price: [MIN_PRICE, MAX_PRICE],
    discount: [MIN_DISCOUNT_PRICE, MAX_DISCOUNT_PRICE]
  });
  const [data, callProductApi, isLoading] = useApiCall(getFilteredProductsList);
  const [productsData, setProductsData] = useState<ProductsData>({ count: 0, products: [] });
  const [isDeleteModalShown, setIsDeleteModalShown] = useState(false);
  const [productInModal, setProductInModal] = useState<ProductInModal>(INITIAL_PRODUCT_IN_MODAL);
  const [modalProperties, setModalProperties] = useState<ModalProperties>({
    isModalShown: false,
    action: MODAL_TYPE.CREATE.NAME
  });

  useEffect(() => {
    callProductApi([filters]);
  }, []);

  useEffect(() => {
    if (data) {
      setProductsData(data);
    }
  }, [data]);

  const applyFilters = (limit: number, currentPage: number) => {
    callProductApi([{ ...filters, limit, currentPage }]);
    onFiltersChange('limit', limit);
    onFiltersChange('currentPage', currentPage);
  };

  const onFiltersChange = (name: string, value: string | number | number[]) => {
    setFilters((prev: Filters): Filters => {
      return {
        ...prev,
        [name]: value
      };
    });
  };

  const onExportFilteredProductsClick = async () => {
    const data = productsData.products;
    const fileName = 'filtered-products';
    const exportType = exportFromJSON.types.xml;

    exportFromJSON({ data, fileName, exportType });
  };

  const onExportAllProductsClick = async () => {
    const data = await getProductsList();
    const fileName = 'products';
    const exportType = exportFromJSON.types.xml;

    exportFromJSON({ data, fileName, exportType });
  };

  const onSaveProductsFromDummyClick = async () => {
    await saveProductsFromDummy();
    callProductApi([filters]);
  };

  return (
    <div className="home">
      <DeleteModal
        id={productInModal.id}
        modalDeletePropertiesState={{
          isDeleteModalShown: isDeleteModalShown,
          setIsDeleteModalShown: setIsDeleteModalShown
        }}
        recallProductApi={() => callProductApi([filters])}
      />
      <ProductModal
        modalPropertiesState={{
          modalProperties: modalProperties,
          setModalProperties: setModalProperties
        }}
        productInModalState={{
          productInModal: productInModal,
          setProductInModal: setProductInModal
        }}
        recallProductApi={() => {
          callProductApi([filters]);
          setProductInModal(INITIAL_PRODUCT_IN_MODAL);
        }}
      />
      <div className="homeFiltersContainer">
        <div className="homeFilters">
          <FilterPanel
            applyFilters={applyFilters}
            filtersProperties={{
              filters: filters,
              onFiltersChange: onFiltersChange
            }}
          />
        </div>
        <div className="homeTable">
          <ActionButtons
            onOpenProductModalClick={() =>
              setModalProperties({
                action: MODAL_TYPE.CREATE.NAME,
                isModalShown: true
              })
            }
            onExportFilteredProductsClick={onExportFilteredProductsClick}
            onExportAllProductsClick={onExportAllProductsClick}
            onSaveProductsFromDummyClick={onSaveProductsFromDummyClick}
          />
          {!isLoading ? (
            <BasicTable
              onActionButtonClick={(value: ProductInModal) => setProductInModal(value)}
              onOpenProductModalClick={() =>
                setModalProperties({
                  action: MODAL_TYPE.UPDATE.NAME,
                  isModalShown: true
                })
              }
              applyFilters={applyFilters}
              limit={filters.limit}
              currentPage={filters.currentPage}
              productsData={productsData}
              onOpenDeleteModalShown={() => setIsDeleteModalShown(true)}
            />
          ) : (
            <CircularProgress />
          )}
        </div>
      </div>
    </div>
  );
};

export default HomePage;
