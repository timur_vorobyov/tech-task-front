export interface Filters {
  limit: number;
  currentPage: number;
  search: string;
  rating: number;
  price: number[];
  discount: number[];
}

export interface ModalDeletePropertiesState {
  isDeleteModalShown: boolean;
  setIsDeleteModalShown: (isDeleteModalShown: boolean) => void;
}

export interface ProductInModal {
  id: number;
  title: string;
  description: string;
  price: string;
  discountPercentage: string;
  rating: string;
  stock: string;
  brand: string;
  category: string;
  thumbnail: File | null;
  images: File[] | [];
}

export type ModalProperties = {
  isModalShown: boolean;
  action: string;
};

export type ModalPropertiesState = {
  modalProperties: ModalProperties;
  setModalProperties: (modalProperties: ModalProperties) => void;
};
